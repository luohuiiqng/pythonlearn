#打开文件的模式
#'w'写模式，打开时会将文件内容清除
#'r'读模式
#'a'附加模式
#'r+'读写模式
#读取所有内容
with open('pi_digits.txt') as file_object:
    contents =  file_object.read()
print(contents)

#逐行读取
filename = 'pi_digits.txt'
with open(filename) as file_object:
    for line in file_object:
        print(line)
#向文件写入内容
count = 10
with open(filename,'w')  as file_object:
    while count > 0:
        file_object.write("I love you!\n")
        count -= 1