import csv
import matplotlib.pyplot as plt
from datetime import datetime

filename = "C:/Users/1/Documents/仿真/源代码文件/chapter_16/the_csv_file_format/data/sitka_weather_2018_simple.csv"

with open(filename) as f:
    reader = csv.reader(f)
    header_row = next(reader)
    
    #从文件中获取最高温度
    dates,highs,lows = [],[],[]
    for row in reader:
        try:
            high = int(row[5])
            highs.append(high)
            current_date = datetime.strptime(row[2],'%Y-%m-%d')
            dates.append(current_date)
            low = int(row[6])
            lows.append(low)
        except ValueError:
            print(f"Missing data for {current_date}")
print(highs)

#根据最高温度绘制图形
plt.style.use('seaborn')
plt.rcParams["font.sans-serif"]=["SimHei"] #设置字体
plt.rcParams["axes.unicode_minus"]=False #该语句解决图像中的“-”负号的乱码问题

fig,ax = plt.subplots()
ax.plot(dates,highs,c='red',alpha = 0.5)
ax.plot(dates,lows,c='blue',alpha = 0.5)

ax.fill_between(dates,highs,lows,facecolor='blue',alpha=0.1)

#设置图形的格式
ax.set_title('2018年7月每日最高温度',fontsize = 24)
ax.set_xlabel('',fontsize=16)
ax.set_ylabel('温度（F）',fontsize = 16)
ax.tick_params(axis='both',which='major',labelsize=16)

plt.show()
