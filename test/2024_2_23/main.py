import matplotlib.pyplot as plt
from random_walk import RandomWalk

#设置内置样式的代码应该在设置字体的前面，否则会产生中文乱码
plt.style.use('seaborn-v0_8-dark')
#解决中文乱码问题

plt.rcParams["font.sans-serif"]=["SimHei"] #设置字体
plt.rcParams["axes.unicode_minus"]=False #该语句解决图像中的“-”负号的乱码问题


#创建一个RandomWalk
rw = RandomWalk()

rw.fill_walk()

#将所有的点都绘制出来
fig,ax = plt.subplots()
ax.scatter(rw.x_values,rw.y_values,s=15)
plt.show()