import json

#探索数据的结构
filename = 'C:/Users/1/Documents/仿真/源代码文件/chapter_16/mapping_global_data_sets/data/eq_data_1_day_m1.json'

with open(filename) as f:
    all_eq_data = json.load(f)

readable_file = 'C:/Users/1/Documents/仿真/源代码文件/chapter_16/mapping_global_data_sets/data/readable_eq_data.json'
with open(readable_file,'w') as f:
    json.dump(all_eq_data,f,indent=4)

all_eq_dicts = all_eq_data['features']
print(len(all_eq_dicts))

#提取地震等级
mags,titles,lons,lats = [],[],[],[]
for eq_dict in all_eq_dicts:
    mag = eq_dict['properties']['mag']
    title = eq_dict['properties']['title']
    lon = eq_dict['geometry']['coordinates'][0]
    lat = eq_dict['geometry']['coordinates'][1]
    mags.append(mag)
    titles.append(title)
    lons.append(lon)
    lats.append(lat)


print(mags[:10])
print(titles[:2])
print(lons[:5])
print(lats[:5])

#提取位置数据
