'''
自动执行API调用并处理结果，以找出gitHub上星级最高的Python项目
'''
import requests
from plotly.graph_objs import Bar
from plotly import offline

url = 'https://api.github.com/search/repositories?q=language:python&sort=stars'
headers = {'Accept':'application/vnd.github.v3+json'}
r = requests.get(url,headers=headers)
print(f"Status code:{r.status_code}")
#将API响应赋给一个变量
response_dict = r.json()

#处理结果
print(response_dict.keys())

#输出有关仓库的信息
repo_dicts = response_dict['items']
print(f"Total repositories:{response_dict['total_count']}")

#输出第一个仓库
repo_dict = repo_dicts[0]
print(f"\nKeys:{len(repo_dict)}")
for key in sorted(repo_dict.keys()):
    print(key)

 

print("\nSelected information about first repository:")
print(f"Name:{repo_dict['name']}")
print(f"Owner:{repo_dict['owner']['login']}")
print(f"Stars:{repo_dict['stargazers_count']}")
print(f"Repository:{repo_dict['html_url']}")
print(f"Created:{repo_dict['created_at']}")
print(f"Updated:{repo_dict['updated_at']}")
print(f"Description:{repo_dict['description']}")

repo_names,stars,labels = [],[],[]
for repo_dict in repo_dicts:
    repo_names.append(repo_dict['name'])
    stars.append(repo_dict['stargazers_count'])

    owner = repo_dict['owner']['login']
    description = repo_dict['description']
    label = f"{owner}<br/>{description}"
    labels.append(label)

#可视化
data = [{
    'type':'bar',
    'x':repo_names,
    'y':stars,
    'marker':{
        'color':'rgb(60,100,150)',
        'line':{'width':1.5,'color':'rgb(25,25,25)'}        
    },
    'opacity':0.6
}]

my_layout = {
    'title':'github上最受欢迎的python项目',
    'xaxis':{
        'title':'Repository',
        'titlefont':{'size':24},
        'tickfont':{'size':14}
        },
    'yaxis':{
        'title':'Stars',
        'titlefont':{'size':24},
        'tickfont':{'size':14}
        }
}

fig = {'data':data,'layout':my_layout}
offline.plot(fig,filename='python_repos.html')

    