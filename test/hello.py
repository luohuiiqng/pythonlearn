print("hello world!")
people_1 = "xiaohong"
people_2 = "xiaoming"
print(people_1+" love "+people_2)
print(people_1.title()+" love "+people_2)
print(people_1.upper()+ " love "+people_2)
full_name = f"{people_1} love  {people_2}"
print(full_name)
#删除多余的空格
favorite_language = 'python '
print(favorite_language)
print(favorite_language.rstrip())
MAX_AGE = 100
print(MAX_AGE)
MAX_AGE = 101
print(MAX_AGE)
#列表
bicycles = ['吉安','南昌','香港','柳州','成都','苏州','上海']
print(bicycles)
#修改列表元素
bicycles[-1] = '北京'
print(bicycles)
#在列表中添加元素
bicycles.append('上海')
print(bicycles)
#在列表中插入元素
bicycles.insert(0,'宝安')
print(bicycles)
#从列表中删除元素
del bicycles[0]
print(bicycles)
oneCity = bicycles.pop()
twoCity = bicycles.pop(2)
print('oneCity '+oneCity)
print(bicycles)
#删除某个特定值的元素.note 不存在会报错
bicycles.remove('成都')
print(bicycles)
#排序
#---该方法会永久改变列表中的元素顺序
bicycles.sort()
print(bicycles)
bicycles.append('杭州')
print(bicycles)
#---该方法不会
print(sorted(bicycles))
#反转列表元素
bicycles.reverse()
print(bicycles)
#确定列表的长度
print(len(bicycles))
#循环打印列表中的元素
for pp in bicycles:
    print(pp)
for pp in bicycles:
    print(f"{pp.title()} is good city!")
    print(f"This is {str(pp.index)}")
#创建数值列表
#---函数range
print('函数Range:')
for value in range(1,10):
    print(value)
print('通过range创建列表')
numbers = list(range(1,5))
for value in numbers:
    print(f"numbers is {value}")
#获取列表中的最小值
print(f"min is {min(numbers)} ...")
#获取列表中的最大值
print(f"max is {max(numbers)} ...")
#求和
print(f"sum is {sum(numbers)} ...")
#列表解析
squares = [value**2 for value in range(1,11)]
print(squares)
#切片
players = ['hqluo','xiaohong','xiaoming','xiaogang','xiaolan']
print(players[0:3])
#列表的辅助
players2 = players[:]
print(players2)
#元组
dimensios = (200,50)
print(dimensios)
#if
for value in dimensios:
    if value == 200:
        print("find a value...")
    else:
        print("not find a value...")
#字典
alien_0 = {'color':'green','poits':5}
print(alien_0['color'])
print(alien_0['poits'])
#空的字典
alien_0 = {}
alien_0['name'] = "zxs"
print(alien_0)
#删除键值对
del alien_0['name']
print(alien_0)
point_value = alien_0.get('points','No point value assigned.')
print(point_value)
people = {
    'mame':'hqluo',
    'age':18,
    'weight':120,
    "民族":'汉族',
    '性别':'男'    
}
#字典遍历
for key, value in people.items():
    print(f"key:{key},value:{value}")
#用户输入
message = input('Please enter name of love people:')
print(message)
#字符转数字
age = 19
print(int(age))
count = 100
while count > 0:
    print(f"love you {count} year...")
    count-=1
def greet_user(username):
    print(f"Hello,{username.title()}. I love you!")
loves = ['x','y','z','a','b','m']
for value in loves:
    greet_user(value)